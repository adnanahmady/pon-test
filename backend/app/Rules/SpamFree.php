<?php

namespace App\Rules;

use App\Support\Contracts\Detector;
use App\Support\Detectors\InvalidKeywords;
use App\Support\Detectors\InvalidPatterns;
use Illuminate\Contracts\Validation\Rule;

class SpamFree implements Rule
{
    private array $filters = [
        InvalidPatterns::class,
        InvalidKeywords::class,
    ];

    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed  $value
     *
     * @return bool
     */
    public function passes($attribute, $value): bool
    {
        foreach ($this->filters as $filter) {
            if ($this->isInvalid(
                app($filter),
                $value
            )) {
                return false;
            }
        }

        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message(): string
    {
        return 'the :attribute contains spam.';
    }

    /**
     * Detects if given value is invalid or not.
     *
     * @param Detector $filter Filter.
     * @param string   $value  Value.
     *
     * @return bool
     */
    private function isInvalid(
        Detector $filter,
        string $value
    ): bool {
        return $filter->detect($value);
    }
}
