<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\MorphMany;

class Answer extends Model
{
    use HasFactory;

    public const TABLE = 'answers';
    public const ID = 'id';
    public const CONTENT = 'content';
    public const OWNER = 'user_id';
    public const QUESTION = 'question_id';
    public const MARKED_AS_BEST_AT = 'marked_as_best_at';

    protected $table = self::TABLE;

    protected $fillable = [
        self::CONTENT,
        self::OWNER,
        self::QUESTION,
        self::MARKED_AS_BEST_AT
    ];

    /**
     * The person who answered.
     *
     * @return BelongsTo
     */
    public function owner(): BelongsTo
    {
        return $this->belongsTo(User::class, self::OWNER);
    }

    /**
     * The answered question.
     *
     * @return BelongsTo
     */
    public function question(): BelongsTo
    {
        return $this->belongsTo(Question::class);
    }

    /**
     * Marks itself as best answer.
     *
     * @param bool $mark Mark or not the answer as best.
     *
     * @return Answer
     */
    public function markAsBest(
        bool $mark
    ): Answer {
        $this->update([
            self::MARKED_AS_BEST_AT => $mark ? now() : null
        ]);

        return $this;
    }

    /**
     * Comments on this answer.
     *
     * @return MorphMany
     */
    public function comments(): MorphMany
    {
        return $this->morphMany(
            Comment::class,
            Comment::COMMENTABLE
        );
    }
}
