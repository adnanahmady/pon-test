<?php

namespace App\Models;

use App\Support\Constants\VotedQuestionConstant;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\MorphMany;

/**
 * @property-read Collection $votes
 * @property-read Collection $answers
 */
class Question extends Model
{
    use HasFactory;

    public const TABLE = 'questions';
    public const ID = 'id';
    public const TITLE = 'title';
    public const CONTENT = 'content';
    public const OWNER = 'user_id';

    protected $table = self::TABLE;

    protected $fillable = [
        self::OWNER,
        self::TITLE,
        self::CONTENT,
    ];

    /**
     * The user who asked this question.
     *
     * @return BelongsTo
     */
    public function owner(): BelongsTo
    {
        return $this->belongsTo(User::class, self::OWNER);
    }

    /**
     * Users which voted this question.
     *
     * @return BelongsToMany
     */
    public function votes(): BelongsToMany
    {
        return $this->belongsToMany(
            User::class,
            VotedQuestionConstant::TABLE,
            VotedQuestionConstant::QUESTION,
            VotedQuestionConstant::USER,
        );
    }

    /**
     * List of answers for this question.
     *
     * @return HasMany
     */
    public function answers(): HasMany
    {
        return $this->hasMany(Answer::class);
    }

    /**
     * Comments on this question.
     *
     * @return MorphMany
     */
    public function comments(): MorphMany
    {
        return $this->morphMany(
            Comment::class,
            Comment::COMMENTABLE
        );
    }
}
