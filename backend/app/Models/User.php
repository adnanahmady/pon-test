<?php

namespace App\Models;

// use Illuminate\Contracts\Auth\MustVerifyEmail;
use App\Support\Constants\VotedQuestionConstant;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens;
    use HasFactory;
    use Notifiable;

    public const TABLE = 'users';
    public const ID = 'id';
    public const NAME = 'name';
    public const EMAIL = 'email';
    public const PASSWORD = 'password';
    public const EMAIL_VERIFIED_AT  = 'email_verified_at';

    protected $table = self::TABLE;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        self::NAME,
        self::EMAIL,
        self::PASSWORD,
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        self::PASSWORD,
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        self::EMAIL_VERIFIED_AT => 'datetime',
    ];

    /**
     * User created questions.
     *
     * @return HasMany
     */
    public function questions(): HasMany
    {
        return $this->hasMany(
            Question::class,
            Question::OWNER
        );
    }

    /**
     * List of questions which this user asked.
     *
     * @return BelongsToMany
     */
    public function votedQuestions(): BelongsToMany
    {
        return $this->belongsToMany(
            Question::class,
            VotedQuestionConstant::TABLE,
            VotedQuestionConstant::USER,
            VotedQuestionConstant::QUESTION
        );
    }

    /**
     * This user will vote given question.
     *
     * @param Question $question Question.
     *
     * @return void
     */
    public function vote(Question $question): void
    {
        $this->votedQuestions()->attach($question);
    }
}
