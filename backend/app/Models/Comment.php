<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphTo;

/**
 * @property-read Model $commentable
 */
class Comment extends Model
{
    use HasFactory;

    public const TABLE = 'comments';
    public const ID = 'id';
    public const CONTENT = 'content';
    public const COMMENTABLE = 'commentable';

    protected $table = self::TABLE;

    protected $fillable = [
        self::CONTENT,
    ];

    public function commentable(): MorphTo
    {
        return $this->morphTo();
    }
}
