<?php

namespace App\Http\Resources\V1\Question;

use App\Http\Resources\V1\User\OwnerResource;
use App\Models\Question;
use Illuminate\Http\Resources\Json\JsonResource;

class ItemResource extends JsonResource
{
    public const ID = 'id';
    public const TITLE = 'title';
    public const CONTENT = 'content';
    public const CREATED_AT = 'created_at';
    public const UPDATED_AT = 'updated_at';
    public const OWNER = 'owner';

    public function toArray($request): array
    {
        return [
            self::ID => $this->{Question::ID},
            self::TITLE => $this->{Question::TITLE},
            self::CONTENT => $this->{Question::CONTENT},
            self::OWNER => $this->when(
                Question::OWNER,
                new OwnerResource($this->owner)
            ),
            self::CREATED_AT => $this->{Question::CREATED_AT},
            self::UPDATED_AT => $this->{Question::UPDATED_AT},
        ];
    }
}
