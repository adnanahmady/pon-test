<?php

namespace App\Http\Resources\V1\Question;

use App\Models\Question;
use Illuminate\Http\Resources\Json\JsonResource;

class AnsweredResource extends JsonResource
{
    public const ID = 'id';
    public const TITLE = 'title';
    public const CREATED_AT = 'created_at';

    public function toArray($request): array
    {
        return [
            self::ID => $this->{Question::ID},
            self::TITLE => $this->{Question::TITLE},
            self::CREATED_AT => $this->{Question::CREATED_AT},
        ];
    }
}
