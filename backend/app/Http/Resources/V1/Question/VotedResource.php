<?php

namespace App\Http\Resources\V1\Question;

use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;

/**
 * @property-read BelongsToMany votes
 */
class VotedResource extends ItemResource
{
    public const VOTED_NUMBER = 'voted_number';

    public function toArray($request): array
    {
        return array_merge(parent::toArray($request), [
            self::VOTED_NUMBER => $this->votes->count()
        ]);
    }

    public function toResponse($request): JsonResponse
    {
        return parent::toResponse($request)
            ->setStatusCode(Response::HTTP_CREATED)
            ->setEncodingOptions(JSON_UNESCAPED_UNICODE);
    }
}
