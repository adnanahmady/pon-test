<?php

namespace App\Http\Resources\V1\User;

use App\Models\User;
use Illuminate\Http\Resources\Json\JsonResource;

class OwnerResource extends JsonResource
{
    public const ID = 'id';
    public const NAME = 'name';
    public const EMAIL = 'email';

    public function toArray($request): array
    {
        return [
            self::ID => $this->{User::ID},
            self::NAME => $this->{User::ID},
            self::EMAIL => $this->{User::EMAIL},
        ];
    }
}
