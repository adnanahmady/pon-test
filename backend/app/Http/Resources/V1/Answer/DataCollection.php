<?php

namespace App\Http\Resources\V1\Answer;

use Illuminate\Http\Resources\Json\ResourceCollection;
use Illuminate\Support\Collection;

class DataCollection extends ResourceCollection
{
    public function toArray($request): Collection
    {
        return $this->collection->map(
            fn ($item) => new ItemResource($item)
        );
    }
}
