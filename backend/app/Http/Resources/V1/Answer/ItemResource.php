<?php

namespace App\Http\Resources\V1\Answer;

use App\Http\Resources\V1\Question\AnsweredResource;
use App\Http\Resources\V1\User\OwnerResource;
use App\Models\Answer;
use Illuminate\Http\Resources\Json\JsonResource;

class ItemResource extends JsonResource
{
    public const ID = 'id';
    public const CONTENT = 'content';
    public const OWNER = 'owner';
    public const QUESTION = 'question';
    public const CREATED_AT = 'created_at';
    public const UPDATED_AT = 'updated_at';

    /**
     * @var Answer
     */
    public $resource;

    public function toArray($request): array
    {
        return [
            self::ID => $this->{Answer::ID},
            self::CONTENT => $this->{Answer::CONTENT},
            self::OWNER => $this->whenLoaded(
                'owner',
                new OwnerResource($this->owner)
            ),
            self::QUESTION => $this->whenLoaded(
                'question',
                new AnsweredResource($this->question)
            ),
            self::CREATED_AT => $this->{Answer::CREATED_AT},
            self::UPDATED_AT => $this->{Answer::UPDATED_AT},
        ];
    }
}
