<?php

namespace App\Http\Resources\V1\Comment;

use App\Models\Answer;
use Illuminate\Http\Resources\Json\JsonResource;

class AnswerResource extends JsonResource
{
    public const ID = 'id';
    public const CONTENT = 'content';

    public function toArray($request): array
    {
        return [
            self::ID => $this->{Answer::ID},
            self::CONTENT => $this->{Answer::CONTENT}
        ];
    }
}
