<?php

namespace App\Http\Resources\V1\Comment;

use App\Models\Question;
use Illuminate\Http\Resources\Json\JsonResource;

class QuestionResource extends JsonResource
{
    public const ID = 'id';
    public const CONTENT = 'content';

    public function toArray($request): array
    {
        return [
            self::ID => $this->{Question::ID},
            self::CONTENT => $this->{Question::CONTENT}
        ];
    }
}
