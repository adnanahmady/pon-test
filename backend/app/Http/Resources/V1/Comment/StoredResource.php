<?php

namespace App\Http\Resources\V1\Comment;

use App\Models\Answer;
use App\Models\Comment;
use App\Models\Question;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Resources\MissingValue;
use Symfony\Component\HttpFoundation\Response;

class StoredResource extends JsonResource
{
    public const ID = 'id';
    public const CONTENT = 'content';
    public const ANSWER = 'answer';
    public const CREATED_AT = 'created_at';
    public const UPDATED_AT = 'updated_at';
    public const QUESTION = 'question';

    /**
     * @var Comment
     */
    public $resource;

    public function toArray($request): array
    {
        return [
            self::ID => $this->{Comment::ID},
            self::CONTENT => $this->{Comment::CONTENT},
            self::ANSWER => $this->getCommentable(
                Answer::class,
                AnswerResource::class
            ),
            self::QUESTION => $this->getCommentable(
                Question::class,
                QuestionResource::class
            ),
            self::CREATED_AT => $this->{Comment::CREATED_AT},
            self::UPDATED_AT => $this->{Comment::UPDATED_AT},
        ];
    }

    public function toResponse($request): JsonResponse
    {
        return parent::toResponse($request)
            ->setEncodingOptions(JSON_UNESCAPED_UNICODE)
            ->setStatusCode(Response::HTTP_CREATED);
    }

    /**
     * @param string $class    Commentable class type.
     * @param string $resource Commentable resource.
     *
     * @return QuestionResource|AnswerResource|MissingValue
     */
    private function getCommentable(
        string $class,
        string $resource
    ): QuestionResource|AnswerResource|MissingValue {
        $commentable = $this->resource->commentable;

        return is_a($commentable, $class) ?
            new $resource($commentable) :
            new MissingValue();
    }
}
