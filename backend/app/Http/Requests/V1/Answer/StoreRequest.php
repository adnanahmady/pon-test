<?php

namespace App\Http\Requests\V1\Answer;

use App\Http\Requests\AbstractFormRequest;
use App\Models\Question;

class StoreRequest extends AbstractFormRequest
{
    public const CONTENT = 'content';
    public const QUESTION_ID = 'question_id';

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            self::CONTENT => 'required|min:2',
            self::QUESTION_ID => sprintf(
                'required|exists:%s,%s',
                Question::TABLE,
                Question::ID
            )
        ];
    }
}
