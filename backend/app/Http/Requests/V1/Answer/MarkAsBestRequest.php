<?php

namespace App\Http\Requests\V1\Answer;

use App\Http\Requests\AbstractFormRequest;
use App\Models\User;

class MarkAsBestRequest extends AbstractFormRequest
{
    public const IS_BEST = 'is_best';

    public function authorize(): bool
    {
        return parent::authorize() && $this->isTheQuestionOwner();
    }

    private function isTheQuestionOwner(): bool
    {
        return $this->getUserId() === $this->getQuestionOwnerId();
    }

    private function getUserId(): int
    {
        return $this->user()->{User::ID};
    }

    private function getQuestionOwnerId(): int
    {
        return $this->answer->question->owner->{User::ID};
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules(): array
    {
        return [
            self::IS_BEST => 'required|boolean',
        ];
    }
}
