<?php

namespace App\Http\Requests\V1\Comment;

use App\Http\Requests\AbstractFormRequest;

class StoreRequest extends AbstractFormRequest
{
    public const CONTENT = 'content';

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules(): array
    {
        return [
            self::CONTENT => 'required|spamFree',
        ];
    }
}
