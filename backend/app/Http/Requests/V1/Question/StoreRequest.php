<?php

namespace App\Http\Requests\V1\Question;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    public const TITLE = 'title';
    public const CONTENT = 'content';

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules(): array
    {
        return [
            self::TITLE => 'required|min:3|max:200',
            self::CONTENT => 'required|min:3'
        ];
    }
}
