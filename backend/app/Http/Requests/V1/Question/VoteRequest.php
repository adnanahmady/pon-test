<?php

namespace App\Http\Requests\V1\Question;

use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Gate;

class VoteRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return Gate::allows(
            'vote',
            $this->question
        );
    }

    /**
     * If authorization fails, this method will
     * be triggered and proper exception with
     * given message will be shown to the client.
     *
     * @throws AuthorizationException
     *
     * @return void
     */
    protected function failedAuthorization(): void
    {
        throw new AuthorizationException(
            'You are not authorized to vote answered questions.'
        );
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules(): array
    {
        return [
            //
        ];
    }
}
