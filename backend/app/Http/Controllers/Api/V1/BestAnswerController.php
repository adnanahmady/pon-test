<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Http\Requests\V1\Answer\MarkAsBestRequest;
use App\Http\Resources\V1\Answer\BestResource;
use App\Models\Answer;

class BestAnswerController extends Controller
{
    public function update(
        MarkAsBestRequest $request,
        Answer $answer
    ): BestResource {
        return new BestResource(
            $answer->markAsBest(
                $request->{MarkAsBestRequest::IS_BEST}
            )
        );
    }
}
