<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Http\Requests\V1\Answer\StoreRequest;
use App\Http\Resources\V1\Answer\DataCollection;
use App\Http\Resources\V1\Answer\StoredResource;
use App\Models\Answer;
use App\Models\User;

class AnswerController extends Controller
{
    public function index(): DataCollection
    {
        return new DataCollection(
            Answer::with('owner', 'question')->get()
        );
    }

    public function store(StoreRequest $request): StoredResource
    {
        return new StoredResource(
            Answer::create([
                Answer::OWNER => $request->user()->{User::ID},
                Answer::QUESTION => $request->{StoreRequest::QUESTION_ID},
                Answer::CONTENT => $request->{StoreRequest::CONTENT},
            ])
        );
    }
}
