<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Http\Requests\V1\Question\StoreRequest;
use App\Http\Resources\V1\Question\DataCollection;
use App\Http\Resources\V1\Question\StoredResource;
use App\Models\Question as Model;
use App\Models\User;
use Illuminate\Http\Request;

class QuestionController extends Controller
{
    public function index(): DataCollection
    {
        return new DataCollection(Model::all());
    }

    public function show(Request $request): DataCollection
    {
        return new DataCollection($request->user()->questions);
    }

    public function store(
        StoreRequest $request
    ): StoredResource {
        return new StoredResource(
            Model::create([
                Model::OWNER => $request->user()->{User::ID},
                Model::TITLE => $request->{Model::TITLE},
                Model::CONTENT => $request->{Model::CONTENT},
            ])
        );
    }
}
