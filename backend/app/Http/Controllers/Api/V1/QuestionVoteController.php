<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Http\Requests\V1\Question\VoteRequest;
use App\Http\Resources\V1\Question\VotedResource;
use App\Models\Question;

class QuestionVoteController extends Controller
{
    /**
     * Voting process is getting handled.
     *
     * @param VoteRequest $request  Contains authorization logic.
     * @param Question    $question Voted Question.
     *
     * @return VotedResource
     */
    public function store(
        VoteRequest $request,
        Question $question
    ): VotedResource {
        $request->user()->vote($question);

        return new VotedResource($question->fresh());
    }
}
