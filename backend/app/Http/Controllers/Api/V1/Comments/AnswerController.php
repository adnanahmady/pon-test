<?php

namespace App\Http\Controllers\Api\V1\Comments;

use App\Http\Controllers\Controller;
use App\Http\Requests\V1\Comment\StoreRequest;
use App\Http\Resources\V1\Comment\StoredResource;
use App\Models\Answer;
use App\Models\Comment;
use Illuminate\Http\Request;

class AnswerController extends Controller
{
    /**
     * Stores user's comment for the answer.
     *
     * @param StoreRequest $request Request.
     * @param Answer       $answer  Answer.
     *
     * @return StoredResource
     */
    public function store(
        StoreRequest $request,
        Answer $answer
    ): StoredResource {
        return new StoredResource(
            $answer->comments()->save(
                new Comment($request->only(StoreRequest::CONTENT))
            )
        );
    }
}
