<?php

namespace App\Http\Controllers\Api\V1\Comments;

use App\Http\Controllers\Controller;
use App\Http\Requests\V1\Comment\StoreRequest;
use App\Http\Resources\V1\Comment\StoredResource;
use App\Models\Comment;
use App\Models\Question;
use Illuminate\Http\Request;

class QuestionController extends Controller
{
    /**
     * Stores user's comment for the question.
     *
     * @param StoreRequest $request  Request.
     * @param Question     $question Question.
     *
     * @return StoredResource
     */
    public function store(
        StoreRequest $request,
        Question $question
    ): StoredResource {
        return new StoredResource(
            $question->comments()->save(
                new Comment($request->only(StoreRequest::CONTENT))
            )
        );
    }
}
