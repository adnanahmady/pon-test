<?php

namespace App\Providers;

use App\Rules\SpamFree;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot(): void
    {
        Validator::extend(
            'spamFree',
            sprintf('%s@passes', SpamFree::class)
        );
    }
}
