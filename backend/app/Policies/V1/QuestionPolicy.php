<?php

namespace App\Policies\V1;

use App\Models\Question;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class QuestionPolicy
{
    use HandlesAuthorization;

    /**
     * Can this question be voted.
     *
     * @param User     $user     Authenticated user.
     * @param Question $question Question.
     *
     * @return bool
     */
    public function vote(User $user, Question $question): bool
    {
        return !$question->answers->count();
    }
}
