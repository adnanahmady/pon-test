<?php

namespace App\Support\Contracts;

interface Detector
{
    /**
     * Detects if given text is valid or not.
     *
     * @param string $text Text.
     *
     * @return bool
     */
    public function detect(string $text): bool;
}
