<?php

namespace App\Support\Constants;

interface VotedQuestionConstant
{
    public const TABLE = 'user_voted_question';
    public const USER = 'user_id';
    public const QUESTION = 'question_id';
}
