<?php

namespace App\Support\Detectors;

use App\Support\Contracts\Detector;

class InvalidPatterns implements Detector
{
    private array $patterns = [
        '[<>\/]',
    ];

    public function detect(string $text): bool
    {
        foreach ($this->patterns as $pattern) {
            if (preg_match("/$pattern/", $text)) {
                return true;
            }
        }

        return false;
    }
}
