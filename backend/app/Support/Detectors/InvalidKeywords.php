<?php

namespace App\Support\Detectors;

use App\Support\Contracts\Detector;

class InvalidKeywords implements Detector
{
    private array $keywords = [
        'invalid',
    ];

    public function detect(string $text): bool
    {
        foreach ($this->keywords as $keyword) {
            if (stripos($text, $keyword) !== false) {
                return true;
            }
        }

        return false;
    }
}
