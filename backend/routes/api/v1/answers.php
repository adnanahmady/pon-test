<?php

use Illuminate\Support\Facades\Route;

Route::get('', [
    \App\Http\Controllers\Api\V1\AnswerController::class,
    'index'
])->name('all');
Route::post('', [
    \App\Http\Controllers\Api\V1\AnswerController::class,
    'store'
])->name('store');
Route::patch('{answer}/best', [
    \App\Http\Controllers\Api\V1\BestAnswerController::class,
    'update'
])->name('best');
Route::post('/{answer}/comments', [
    \App\Http\Controllers\Api\V1\Comments\AnswerController::class,
    'store'
])->name('comments');
