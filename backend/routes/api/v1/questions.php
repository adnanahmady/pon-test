<?php

use Illuminate\Support\Facades\Route;

Route::post('{question}/vote', [
    \App\Http\Controllers\Api\V1\QuestionVoteController::class,
    'store'
])->name('vote');
Route::get('owned', [
    \App\Http\Controllers\Api\V1\QuestionController::class,
    'show'
])->name('owned');
Route::get('', [
    \App\Http\Controllers\Api\V1\QuestionController::class,
    'index'
])->name('all');
Route::post('', [
    \App\Http\Controllers\Api\V1\QuestionController::class,
    'store'
])->name('store');
Route::post('/{question}/comments', [
    \App\Http\Controllers\Api\V1\Comments\QuestionController::class,
    'store'
])->name('comments');
