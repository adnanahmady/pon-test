<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::middleware('auth:sanctum')
    ->prefix('v1')
    ->name('v1.')
    ->namespace('V1')
    ->group(function () {
        Route::prefix('questions')
            ->name('questions.')
            ->group(base_path('routes/api/v1/questions.php'));
        Route::prefix('answers')
            ->name('answers.')
            ->group(base_path('routes/api/v1/answers.php'));
    });
