<?php

namespace Tests\Feature\Comment\Question;

use App\Http\Requests\V1\Comment\StoreRequest;
use App\Http\Resources\V1\Comment\QuestionResource;
use App\Http\Resources\V1\Comment\StoredResource;
use App\Models\Question;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Testing\TestResponse;
use Illuminate\Validation\ValidationException;
use Tests\TestCase;
use Tests\Traits\MigrateDatabase;

class CreateTest extends TestCase
{
    use MigrateDatabase;

    public function dataProviderForValidation(): array
    {
        return [
            StoreRequest::CONTENT . ' must exist' => [[]],
            sprintf(
                'only expected characters are allowed for %s',
                StoreRequest::CONTENT
            ) => [[StoreRequest::CONTENT => '<tag>$ss<?php ?>']],
        ];
    }

    /**
     * @dataProvider dataProviderForValidation
     *
     * @param array $data Data.
     *
     * @return void
     *
     * @test
     */
    public function validation(array $data): void
    {
        $this->withoutExceptionHandling();
        $this->expectException(ValidationException::class);
        $this->actingAs(user());
        $this->request($data);
    }
    /** @test */
    public function commented_question_is_mentioned(): void
    {
        $this->withoutExceptionHandling();
        $this->actingAs(user());
        $question = $this->request()
            ->json('data.' . StoredResource::QUESTION);

        $this->assertArrayHasKeys([
            QuestionResource::ID,
            QuestionResource::CONTENT,
        ], $question);
        $this->assertArrayNotHasKey(Question::OWNER, $question);
        $this->assertIsInt($question[QuestionResource::ID]);
        $this->assertIsString($question[QuestionResource::CONTENT]);
    }

    /** @test */
    public function persian_characters_are_not_escaped(): void
    {
        $this->withoutExceptionHandling();
        $this->actingAs(user());
        $text = "به نظر من این پاسخ دارای اشکالات زیادی است.";
        $content = $this->request([
            StoreRequest::CONTENT => $text
        ])->content();
        $this->assertStringContainsString(
            $text,
            $content
        );
    }

    /** @test */
    public function data_should_not_contain_unexpected_fields(): void
    {
        $this->withoutExceptionHandling();
        $this->actingAs(user());
        $data = $this->request()->json('data');

        $this->assertArrayNotHasKey(
            StoredResource::ANSWER,
            $data
        );
    }

    /** @test */
    public function data_must_contain_expected_fields(): void
    {
        $this->withoutExceptionHandling();
        $this->actingAs(user());
        $data = $this->request()->json('data');

        $this->assertArrayHasKeys([
            StoredResource::ID,
            StoredResource::CONTENT,
            StoredResource::QUESTION,
            StoredResource::CREATED_AT,
            StoredResource::UPDATED_AT,
        ], $data);
        $this->assertIsInt($data[StoredResource::ID]);
        $this->assertIsString($data[StoredResource::CONTENT]);
        $this->assertIsArray($data[StoredResource::QUESTION]);
        $this->assertIsString($data[StoredResource::CREATED_AT]);
        $this->assertIsString($data[StoredResource::UPDATED_AT]);
    }

    /** @test */
    public function only_authenticated_users_can_comment_an_question(): void
    {
        $this->withoutExceptionHandling();
        $this->expectException(AuthenticationException::class);
        $this->request();
    }

    /** @test */
    public function user_can_comment_an_question(): void
    {
        $this->actingAs(user());
        $this->request()->assertCreated();
    }

    private function request(
        array $data = [
            StoreRequest::CONTENT => 'i think this question is great',
        ],
        Question $question = null
    ): TestResponse {
        return $this->postJson(route(
            'v1.questions.comments',
            [
                'question' => $question ?? create(Question::class),
            ] + $data
        ));
    }
}
