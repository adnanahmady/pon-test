<?php

namespace Tests\Feature\Answer;

use App\Http\Resources\V1\Answer\ItemResource;
use App\Http\Resources\V1\Question\AnsweredResource;
use App\Http\Resources\V1\User\OwnerResource;
use App\Models\Answer;
use App\Models\Question;
use App\Models\User;
use Illuminate\Auth\AuthenticationException;
use Tests\TestCase;
use Tests\Traits\MigrateDatabase;

class GetTest extends TestCase
{
    use MigrateDatabase;

    /** @test */
    public function guests_can_not_see_answers()
    {
        $this->withoutExceptionHandling();
        $this->expectException(AuthenticationException::class);

        create(Answer::class, 6);
        $this->getJson(route('v1.answers.all'));
    }

    /** @test */
    public function owner_must_contain_expected_information()
    {
        $this->actingAs(create(User::class));
        create(Answer::class, 6);
        $item = $this->getJson(
            route('v1.answers.all')
        )->json('data.5.' . ItemResource::OWNER);

        $this->assertArrayHasKeys([
            OwnerResource::ID,
            OwnerResource::NAME,
            OwnerResource::EMAIL,
        ], $item);
        $this->assertArrayNotHasKeys([
            User::UPDATED_AT,
            User::PASSWORD,
            User::EMAIL_VERIFIED_AT,
        ], $item);
    }

    /** @test */
    public function question_must_contain_expected_information()
    {
        $this->actingAs(create(User::class));
        create(Answer::class, 6);
        $item = $this->getJson(
            route('v1.answers.all')
        )->json('data.5.' . ItemResource::QUESTION);

        $this->assertArrayHasKeys([
            AnsweredResource::ID,
            AnsweredResource::TITLE,
            AnsweredResource::CREATED_AT,
        ], $item);
        $this->assertArrayNotHasKeys([
            Question::CONTENT,
            Question::UPDATED_AT,
        ], $item);
    }

    /** @test */
    public function response_contains_expected_information(): void
    {
        $this->actingAs(create(User::class));
        create(Answer::class, 6);
        $item = $this->getJson(
            route('v1.answers.all')
        )->json('data.5');

        $this->assertArrayHasKeys([
            ItemResource::ID,
            ItemResource::CONTENT,
            ItemResource::OWNER,
            ItemResource::QUESTION,
            ItemResource::CREATED_AT,
            ItemResource::UPDATED_AT,
        ], $item);
        $this->assertIsInt($item[ItemResource::ID]);
        $this->assertIsString($item[ItemResource::CONTENT]);
        $this->assertIsString($item[ItemResource::CREATED_AT]);
        $this->assertIsString($item[ItemResource::UPDATED_AT]);
        $this->assertIsArray($item[ItemResource::OWNER]);
        $this->assertIsArray($item[ItemResource::QUESTION]);
        $this->assertArrayNotHasKeys([
            Answer::OWNER,
            Answer::QUESTION,
        ], $item);
    }

    /** @test */
    public function users_may_see_a_list_of_answers(): void
    {
        $this->actingAs(create(User::class));

        $this->getJson(route('v1.answers.all'))->assertOk();
    }
}
