<?php

namespace Tests\Feature\Answer;

use App\Http\Requests\V1\Answer\MarkAsBestRequest;
use App\Http\Resources\V1\Answer\BestResource;
use App\Models\Answer;
use App\Models\Question;
use App\Models\User;
use Illuminate\Auth\Access\AuthorizationException;
use Tests\TestCase;
use Tests\Traits\MigrateDatabase;

class BestAnswerTest extends TestCase
{
    use MigrateDatabase;

    /** @test */
    public function the_answer_with_updated_information_is_given_back(): void
    {
        $this->actingAs($user = create(User::class));

        $data = $this->patchJson(
            route('v1.answers.best', [
                'answer' => create(Answer::class, [
                    Answer::QUESTION => create(Question::class, [
                        Question::OWNER => $user
                    ])
                ])
            ]),
            [MarkAsBestRequest::IS_BEST => true]
        )->json('data');

        $this->assertArrayHasKeys([
            BestResource::ID,
            BestResource::CONTENT,
            BestResource::QUESTION,
            BestResource::MARKED_AS_BEST_AT,
            BestResource::CREATED_AT,
            BestResource::UPDATED_AT,
        ], $data);
        $this->assertIsInt($data[BestResource::ID]);
        $this->assertIsString($data[BestResource::CONTENT]);
        $this->assertIsArray($data[BestResource::QUESTION]);
        $this->assertIsString($data[BestResource::MARKED_AS_BEST_AT]);
        $this->assertIsString($data[BestResource::CREATED_AT]);
        $this->assertIsString($data[BestResource::UPDATED_AT]);
    }

    /** @test */
    public function it_can_be_marked_as_best_answer_by_question_owner(): void
    {
        $this->actingAs($user = create(User::class));

        $this->patchJson(
            route('v1.answers.best', [
                'answer' => create(Answer::class, [
                    Answer::QUESTION => create(Question::class, [
                        Question::OWNER => $user
                    ])
                ])
            ]),
            [MarkAsBestRequest::IS_BEST => true]
        )->assertOK();
    }

    /** @test */
    public function only_the_user_who_asked_the_question_can_mark_answer_as_best(): void
    {
        $this->withoutExceptionHandling();
        $this->expectException(AuthorizationException::class);
        $this->actingAs(create(User::class));

        $this->patchJson(
            route('v1.answers.best', [
                'answer' => create(Answer::class)
            ]),
            [MarkAsBestRequest::IS_BEST => true]
        );
    }
}
