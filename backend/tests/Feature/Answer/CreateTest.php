<?php

namespace Tests\Feature\Answer;

use App\Http\Requests\V1\Answer\StoreRequest;
use App\Http\Resources\V1\Answer\StoredResource;
use App\Http\Resources\V1\Question\AnsweredResource;
use App\Models\Answer;
use App\Models\Question;
use App\Models\User;
use Illuminate\Validation\ValidationException;
use Tests\TestCase;
use Tests\Traits\MigrateDatabase;

class CreateTest extends TestCase
{
    use MigrateDatabase;

    private function dataProviderForValidationTest(): array
    {
        return [
            StoreRequest::CONTENT . ' is required' => [[
                StoreRequest::QUESTION_ID => 1
            ]],
            StoreRequest::CONTENT . ' must be at least 2 characters' => [[
                StoreRequest::CONTENT => 'd',
                StoreRequest::QUESTION_ID => 1
            ]],
            'the question that is answered must be mentioned' => [[
                StoreRequest::CONTENT => 'dummy text'
            ]],
            'the question must exist' => [[
                StoreRequest::CONTENT => 'dummy text',
                StoreRequest::QUESTION_ID => 100
            ]],
        ];
    }

    /**
     * @dataProvider dataProviderForValidationTest
     *
     * @param array $data Data.
     *
     * @test
     */
    public function only_answer_with_correct_required_data_can_be_created(
        array $data
    ): void {
        $this->withoutExceptionHandling();
        $this->expectException(ValidationException::class);

        if (@$data['question_id'] && $data['question_id'] < 100) {
            $data['question_id'] = create(Question::class)->{Question::ID};
        }

        $this->actingAs(create(User::class));
        $this->postJson(
            route('v1.answers.store'),
            $data
        );
    }

    /** @test */
    public function question_information_must_contain_expected_information(): void
    {
        $this->actingAs(create(User::class));

        $question = $this->postJson(
            route('v1.answers.store'),
            raw(Answer::class)
        )->json('data.question');

        $this->assertArrayHasKeys([
            AnsweredResource::ID,
            AnsweredResource::TITLE,
            AnsweredResource::CREATED_AT,
        ], $question);
        $this->assertArrayNotHasKeys([
            Question::CONTENT,
            Question::UPDATED_AT,
        ], $question);
    }

    /** @test */
    public function response_must_contain_expected_information(): void
    {
        $this->actingAs(create(User::class));

        $data = $this->postJson(
            route('v1.answers.store'),
            raw(Answer::class)
        )->json('data');

        $this->assertArrayHasKeys([
            StoredResource::ID,
            StoredResource::CONTENT,
            StoredResource::QUESTION,
            StoredResource::CREATED_AT,
            StoredResource::UPDATED_AT,
        ], $data);
        $this->assertArrayNotHasKeys([
            Answer::OWNER,
            'owner'
        ], $data);
    }

    /** @test */
    public function user_may_answer_question(): void
    {
        $this->actingAs(create(User::class));

        $this->postJson(
            route('v1.answers.store'),
            raw(Answer::class)
        )->assertCreated();
    }
}
