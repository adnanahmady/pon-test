<?php

namespace Tests\Feature\Question;

use App\Http\Resources\V1\Question\ItemResource;
use App\Http\Resources\V1\User\OwnerResource;
use App\Models\Question;
use App\Models\User;
use Illuminate\Testing\TestResponse;
use Tests\TestCase;
use Tests\Traits\MigrateDatabase;

class OwnerGetTest extends TestCase
{
    use MigrateDatabase;

    /** @test */
    public function guests_can_not_see_questions(): void
    {
        $this->request()->assertUnauthorized();
    }

    /** @test */
    public function owner_must_contain_expected_information(): void
    {
        create(Question::class, 6);
        $this->actingAs($user = create(User::class));
        create(Question::class, 4, [
            Question::OWNER => $user
        ]);
        $response = $this->request()->json();

        $this->assertCount(4, $response['data']);
        $this->assertArrayHasKeys([
            OwnerResource::ID,
            OwnerResource::NAME,
            OwnerResource::EMAIL,
        ], $response['data'][3][ItemResource::OWNER]);
        $this->assertArrayNotHasKeys([
            User::EMAIL_VERIFIED_AT,
            User::CREATED_AT,
            User::UPDATED_AT,
        ], $response['data'][3][ItemResource::OWNER]);
    }

    /** @test */
    public function response_must_contain_expected_information(): void
    {
        create(Question::class, 6);
        $this->actingAs($user = create(User::class));
        create(Question::class, 4, [
            Question::OWNER => $user
        ]);
        $response = $this->request()->json();

        $this->assertArrayHasKey('data', $response);
        $this->assertArrayHasKeys([
            ItemResource::ID,
            ItemResource::TITLE,
            ItemResource::CONTENT,
            ItemResource::OWNER,
            ItemResource::CREATED_AT,
            ItemResource::UPDATED_AT,
        ], $response['data'][3]);
    }

    /** @test */
    public function user_can_see_its_own_created_questions(): void
    {
        create(Question::class, 6);
        $this->actingAs($user = create(User::class));
        create(Question::class, 6, [
            Question::OWNER => $user
        ]);
        $response = $this->request();
        $response->assertOk();

        $this->assertCount(6, $response->json('data'));
    }

    private function request(): TestResponse
    {
        return $this->getJson(route(
            'v1.questions.owned'
        ));
    }
}
