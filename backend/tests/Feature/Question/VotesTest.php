<?php

namespace Tests\Feature\Question;

use App\Http\Resources\V1\Question\VotedResource;
use App\Models\Answer;
use App\Models\Question;
use App\Models\User;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Testing\TestResponse;
use Tests\TestCase;
use Tests\Traits\MigrateDatabase;

class VotesTest extends TestCase
{
    use MigrateDatabase;

    /** @test */
    public function only_authenticated_user_can_vote(): void
    {
        $this->withoutExceptionHandling();
        $this->expectException(AuthenticationException::class);

        $answered = create(Question::class);
        create(Answer::class, [Answer::QUESTION => $answered]);
        $this->request($answered);
    }

    /** @test */
    public function proper_authorization_message_must_be_shown(): void
    {
        $this->withoutExceptionHandling();
        $this->expectExceptionMessage(
            'You are not authorized to vote answered questions.'
        );

        $answered = create(Question::class);
        create(Answer::class, [Answer::QUESTION => $answered]);
        $this->authRequest($answered);
    }

    /** @test */
    public function user_can_only_vote_questions_which_are_not_answered_yet(): void
    {
        $this->withoutExceptionHandling();
        $this->expectException(AuthorizationException::class);

        $answered = create(Question::class);
        $unAnswered = create(Question::class);
        create(Answer::class, [Answer::QUESTION => $answered]);
        $this->authRequest($answered);
    }

    /** @test */
    public function data_must_contain_specified_fields(): void
    {
        $data = $this->authRequest(
            create(Question::class)
        )->json('data');

        $this->assertArrayHasKeys([
            VotedResource::ID,
            VotedResource::OWNER,
            VotedResource::VOTED_NUMBER,
            VotedResource::TITLE,
            VotedResource::CONTENT,
            VotedResource::CREATED_AT,
            VotedResource::UPDATED_AT,
        ], $data);
    }

    /** @test */
    public function user_can_vote_a_question(): void
    {
        $this->authRequest(
            create(Question::class)
        )->assertCreated();
    }

    /**
     * Request as authenticated user.
     *
     * @param Model $question Question.
     *
     * @return TestResponse
     */
    private function authRequest(Model $question): TestResponse
    {
        $this->actingAs(create(User::class));

        return $this->request($question);
    }

    private function request(Model $question): TestResponse
    {
        return $this->postjson(route(
            'v1.questions.vote',
            ['question' => $question]
        ));
    }
}
