<?php

namespace Tests\Feature\Question;

use App\Http\Requests\V1\Question\StoreRequest;
use App\Http\Resources\V1\Question\StoredResource;
use App\Models\Question;
use App\Models\User;
use Illuminate\Support\Str;
use Illuminate\Validation\ValidationException;
use Tests\TestCase;
use Tests\Traits\MigrateDatabase;

class CreateTest extends TestCase
{
    use MigrateDatabase;

    public function dataProviderForValidationTest(): array
    {
        return [
            StoreRequest::TITLE . ' is required' => [
                [StoreRequest::CONTENT => 'dummy text']
            ],
            StoreRequest::TITLE . ' must be at least 3 characters' => [[
                StoreRequest::TITLE => 'du',
                StoreRequest::CONTENT => 'dummy text',
            ]],
            StoreRequest::TITLE . ' must be 200 characters max' => [[
                StoreRequest::TITLE => Str::random(201),
                StoreRequest::CONTENT => 'dummy text'
            ]],
            StoreRequest::CONTENT . ' is required' => [
                [StoreRequest::TITLE => 'dummy text']
            ],
            StoreRequest::CONTENT . ' must be at least 3 characters' => [[
                StoreRequest::TITLE => 'dummy text',
                StoreRequest::CONTENT => 'du',
            ]],
        ];
    }

    /**
     * @dataProvider dataProviderForValidationTest
     *
     * @param array $fields Fields.
     *
     * @test
     */
    public function only_validated_data_can_be_created(array $fields): void
    {
        $this->withoutExceptionHandling();
        $this->expectException(ValidationException::class);

        $this->actingAs(create(User::class));
        $this->postJson(route(
            'v1.questions.store',
            $fields
        ));
    }

    /** @test */
    public function created_question_must_return_back_expected_information(): void
    {
        $this->actingAs(create(User::class));
        $response = $this->postJson(route(
            'v1.questions.store',
            raw(Question::class)
        ))->json();

        $this->assertArrayHasKey('data', $response);
        $this->assertArrayHasKeys([
            StoredResource::ID,
            StoredResource::TITLE,
            StoredResource::CONTENT,
            StoredResource::CREATED_AT,
        ], $response['data']);
        $this->assertArrayNotHasKey(
            Question::OWNER,
            $response['data']
        );
    }

    /** @test */
    public function user_can_create_questions(): void
    {
        $this->actingAs(create(User::class));

        $this->postJson(route(
            'v1.questions.store',
            raw(Question::class)
        ))->assertCreated();
    }
}
