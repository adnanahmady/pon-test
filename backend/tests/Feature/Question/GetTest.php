<?php

namespace Tests\Feature\Question;

use App\Http\Resources\V1\Question\ItemResource;
use App\Http\Resources\V1\User\OwnerResource;
use App\Models\Question;
use App\Models\User;
use Tests\TestCase;
use Tests\Traits\MigrateDatabase;

class GetTest extends TestCase
{
    use MigrateDatabase;

    /** @test */
    public function guests_can_not_see_questions()
    {
        $this->getJson(route(
            'v1.questions.all'
        ))->assertUnauthorized();
    }

    /** @test */
    public function owner_must_contain_expected_information(): void
    {
        create(Question::class, 6);
        $this->actingAs(create(User::class));

        $response = $this->getJson(route(
            'v1.questions.all'
        ))->json();

        $this->assertArrayHasKeys([
            OwnerResource::ID,
            OwnerResource::NAME,
            OwnerResource::EMAIL,
        ], $response['data'][5][ItemResource::OWNER]);
        $this->assertArrayNotHasKeys([
            User::EMAIL_VERIFIED_AT,
            User::CREATED_AT,
            User::UPDATED_AT,
        ], $response['data'][5][ItemResource::OWNER]);
    }

    /** @test */
    public function response_must_contain_expected_information(): void
    {
        create(Question::class, 6);
        $this->actingAs(create(User::class));

        $response = $this->getJson(route(
            'v1.questions.all'
        ))->json();

        $this->assertArrayHasKey('data', $response);
        $this->assertArrayHasKeys([
            ItemResource::ID,
            ItemResource::TITLE,
            ItemResource::CONTENT,
            ItemResource::OWNER,
            ItemResource::CREATED_AT,
            ItemResource::UPDATED_AT,
        ], $response['data'][5]);
    }

    /** @test */
    public function users_may_see_a_list_of_questions_that_have_already_been_asked(): void
    {
        $this->actingAs(create(User::class));

        $this->getJson(route(
            'v1.questions.all'
        ))->assertOk();
    }
}
