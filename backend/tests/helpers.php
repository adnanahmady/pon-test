<?php

use App\Models\User;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

if (! function_exists('buildFactory')) {
    function buildFactory(
        string $model,
        int $count
    ): Factory {
        $instance = $model::factory();

        if ($count > 0) {
            $instance = $instance->count($count);
        }

        return $instance;
    }
}

if (! function_exists('create')) {
    function create(
        string $model,
        int|array $countOrFields = 0,
        array $fields = []
    ): Model|Authenticatable|Collection {
        if (is_array($countOrFields)) {
            $fields = $countOrFields;
            $countOrFields = 0;
        }

        return buildFactory(
            $model,
            $countOrFields
        )->create($fields);
    }
}

if (! function_exists('raw')) {
    function raw(
        string $model,
        int|array $countOrFields = 0,
        array $fields = []
    ): array {
        if (is_array($countOrFields)) {
            $fields = $countOrFields;
            $countOrFields = 0;
        }

        return buildFactory(
            $model,
            $countOrFields
        )->raw($fields);
    }
}

if (! function_exists('user')) {
    /**
     * Creates a user instance.
     *
     * @return Authenticatable
     */
    function user(): Authenticatable
    {
        return create(User::class);
    }
}
