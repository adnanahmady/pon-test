<?php

namespace Tests;

use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use Tests\Traits\AssertionsTrait;
use Tests\Traits\MigrateDatabase;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;
    use AssertionsTrait;

    protected function setUpTraits()
    {
        $traits = parent::setUpTraits();

        if (isset($traits[MigrateDatabase::class])) {
            $this->migrateDatabase();
        }
    }
}
