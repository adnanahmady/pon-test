<?php

namespace Tests\Traits;

trait AssertionsTrait
{
    protected function assertArrayHasKeys(
        array $keys,
        array $array
    ): void {
        foreach ($keys as $key) {
            $this->assertArrayHasKey($key, $array);
        }
    }

    protected function assertArrayNotHasKeys(
        array $keys,
        array $array
    ): void {
        foreach ($keys as $key) {
            $this->assertArrayNotHasKey($key, $array);
        }
    }
}
