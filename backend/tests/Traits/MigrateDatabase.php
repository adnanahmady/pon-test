<?php

namespace Tests\Traits;

use App\Console\Kernel;

trait MigrateDatabase
{
    protected function migrateDatabase(): void
    {
        $this->artisan('migrate:refresh');
        $this->app[Kernel::class]->setArtisan(null);
    }
}
