<?php

namespace Tests\Unit\Rules;

use App\Rules\SpamFree;
use Tests\TestCase;

class SpamFreeTest extends TestCase
{
    /** @test */
    public function it_can_determine_texts_containing_invalid_patterns(): void
    {
        $this->withoutExceptionHandling();
        $this->assertFalse(
            (new SpamFree())->passes(
                'some',
                'this is an <strong>strong</strong> text'
            )
        );
    }

    /** @test */
    public function it_can_identify_invalid_keywords_in_given_text(): void
    {
        $this->assertFalse(
            (new SpamFree())->passes(
                'some',
                'this is an invalid text'
            )
        );
    }
}
