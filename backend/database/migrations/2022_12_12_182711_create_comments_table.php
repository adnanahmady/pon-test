<?php

use App\Models\Comment;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(Comment::TABLE, function (Blueprint $table) {
            $table->id(Comment::ID);
            $table->text(Comment::CONTENT);
            $table->morphs(Comment::COMMENTABLE);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(Comment::TABLE, function (Blueprint $table) {
            $table->dropMorphs(Comment::COMMENTABLE);
        });
        Schema::dropIfExists(Comment::TABLE);
    }
};
