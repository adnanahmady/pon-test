<?php

use App\Models\Question;
use App\Models\User;
use App\Support\Constants\VotedQuestionConstant;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration implements VotedQuestionConstant {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::create(self::TABLE, function (Blueprint $table) {
            $table->unsignedBigInteger(self::USER);
            $table->unsignedBigInteger(self::QUESTION);
            $table->timestamps();

            $table->foreign(self::USER)
                ->references(User::ID)
                ->on(User::TABLE);
            $table->foreign(self::QUESTION)
                ->references(Question::ID)
                ->on(Question::TABLE);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::table(self::TABLE, function (Blueprint $table) {
            $table->dropForeign([self::USER]);
            $table->dropForeign([self::QUESTION]);
        });
        Schema::dropIfExists(self::TABLE);
    }
};
