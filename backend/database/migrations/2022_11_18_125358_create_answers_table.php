<?php

use App\Models\Answer;
use App\Models\Question;
use App\Models\User;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(Answer::TABLE, function (Blueprint $table) {
            $table->id(Answer::ID);
            $table->string(Answer::CONTENT);
            $table->unsignedBigInteger(Answer::OWNER);
            $table->unsignedBigInteger(Answer::QUESTION);
            $table->timestamps();

            $table->foreign(Answer::OWNER)
                ->references(User::ID)
                ->on(User::TABLE);
            $table->foreign(Answer::QUESTION)
                ->references(Question::ID)
                ->on(Question::TABLE);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(Answer::TABLE, function (Blueprint $table) {
            $table->dropForeign([Answer::OWNER]);
            $table->dropForeign([Answer::QUESTION]);
        });
        Schema::dropIfExists(Answer::TABLE);
    }
};
