<?php

use App\Models\Answer;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(Answer::TABLE, function (Blueprint $table) {
            $table->timestamp(Answer::MARKED_AS_BEST_AT)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(Answer::TABLE, function (Blueprint $table) {
            $table->dropColumn([Answer::MARKED_AS_BEST_AT]);
        });
    }
};
