<?php

use App\Models\Question;
use App\Models\User;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(Question::TABLE, function (Blueprint $table) {
            $table->id(Question::ID);
            $table->unsignedBigInteger(Question::OWNER);
            $table->string(Question::TITLE, 255);
            $table->text(Question::CONTENT);
            $table->timestamps();

            $table->foreign(Question::OWNER)
                ->references(User::ID)
                ->on(User::TABLE);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(Question::TABLE, function (Blueprint $table) {
            $table->dropForeign([Question::OWNER]);
        });
        Schema::dropIfExists(Question::TABLE);
    }
};
