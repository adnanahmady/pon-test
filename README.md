# About

This test performed based on [bootcamp](.assets/Bootcamp.pdf) requirements.
Pay attention that this test has been done using TDD (Test-driven development).

In order to see changes, you should go to [history](https://gitlab.com/adnanahmady/pon-test/-/commits/main/)
section.

# Pre-requirements

- docker
- docker-compose

## Optional Pre-requirements

- make
