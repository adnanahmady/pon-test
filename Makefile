define default
$(if $(1),$(1),$(2))
endef

up:
	@docker-compose up -d

down:
	@docker-compose down

shell:
	@docker-compose exec ${call default,${s},backend} bash

status:
	@docker-compose ps

